--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| Defines a broadcaster capability for sending messages over a network.
--|
--| ### Key Concepts
--| * **Broadcasting**: a mechanism for sending messages over a network.
--| * **Monads for Broadcasting**: specialized monads (`AppM`, `HalogenM`) that provide a way to work with broadcasting in different contexts (e.g., applicative effects, Halogen state management).
--| * **FFI Integration**: the module provides an interface to interact with a low-level library (`Bugout`) using Foreign Function Interface (FFI).
module Domination.Capability.Broadcast where

import Prelude

import Control.Monad.Error.Class (try)
import Control.Monad.Trans.Class (lift)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Domination.AppM (AppM)
import Domination.Capability.Log (class Log, log)
import Effect.Aff (Aff, Error, makeAff)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (class MonadEffect, liftEffect)
import FFI as FFI
import Halogen (HalogenM)

newtype Broadcaster = Broadcaster FFI.Bugout

instance showBroadcaster :: Show Broadcaster where
  show (Broadcaster bugout) = FFI.showBugout bugout

class Monad m <= Broadcast m where
  create :: String -> String -> String -> String -> m (Either Error Broadcaster)
  address :: Broadcaster -> m String
  broadcast :: Broadcaster -> String -> m Unit

instance broadcastHalogenM :: Broadcast m => Broadcast (HalogenM st act slots msg m) where
  create :: String -> String -> String -> String -> (HalogenM st act slots msg m) (Either Error Broadcaster)
  create a b c = lift <<< create a b c
  address :: Broadcaster -> (HalogenM st act slots msg m) String
  address = lift <<< address
  broadcast :: Broadcaster -> String -> (HalogenM st act slots msg m) Unit
  broadcast broadcaster = lift <<< broadcast broadcaster

instance broadcastAppM :: Broadcast AppM where
  create :: String -> String -> String -> String -> AppM (Either Error Broadcaster)
  create a b c = liftAff <<< createBroadcaster a b c
  address :: Broadcaster -> AppM String
  address = liftAff <<< getAddress
  broadcast :: Broadcaster -> String -> AppM Unit
  broadcast broadcaster = liftAff <<< broadcastMessage broadcaster

newtype BroadcastM a = BroadcastM (Aff a)

derive newtype instance functorBroadcastM :: Functor BroadcastM
derive newtype instance applyBroadcastM :: Apply BroadcastM
derive newtype instance applicativeBroadcastM :: Applicative BroadcastM
derive newtype instance bindBroadcastM :: Bind BroadcastM
derive newtype instance monadBroadcastM :: Monad BroadcastM
derive newtype instance monadEffectBroadcastM :: MonadEffect BroadcastM
derive newtype instance monadAffBroadcastM :: MonadAff BroadcastM

instance broadcastBroadcastM :: Broadcast BroadcastM where
  broadcast broadcaster = liftAff <<< broadcastMessage broadcaster
  create a b c = liftAff <<< createBroadcaster a b c
  address = liftAff <<< getAddress

runBroadcastM :: BroadcastM ~> Aff
runBroadcastM (BroadcastM m) = liftAff m

broadcastMessage :: Broadcaster -> String -> Aff Unit
broadcastMessage (Broadcaster bugout) = liftEffect <<< FFI.send bugout

createBroadcaster
  :: String
  -> String
  -> String
  -> String
  -> Aff (Either Error Broadcaster)
createBroadcaster remoteMessageTarget localMessageTarget roomCode announce =
  try
  $ Broadcaster
  <$> makeAff
  (FFI.makeBugout remoteMessageTarget localMessageTarget roomCode announce)

getAddress :: Broadcaster -> Aff String
getAddress (Broadcaster bugout) = liftEffect $ FFI.address bugout

maybeCreateBroadcaster
  :: forall m
  . Log m
  => Broadcast m
  => String
  -> String
  -> String
  -> String
  -> m (Maybe Broadcaster)
maybeCreateBroadcaster roomCode remoteMessageTarget localMessageTarget announce = do
  eBroadcaster <- create roomCode remoteMessageTarget localMessageTarget announce
  case eBroadcaster of
    Left e -> do
      log $ "Error creating broadcaster: " <> show e
      pure Nothing
    Right b -> pure (Just b)
