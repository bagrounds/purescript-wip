--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| Definition of a WireStackExpression data type with various constructors and support for JSON encoding, decoding, and other utilities.
--|
--| ### Key Concepts
--| * Iso abstraction for converting `StackExpression` to `WireStackExpression`
--| * Generic encoding/decoding functionality for `WireStackExpression` using Argonaut
--| * Lens-based data transformations and accessors for `WireStackExpression`
module Domination.Data.Wire.StackExpression where

import Prelude

import Data.Argonaut.Decode.Class (class DecodeJson)
import Data.Argonaut.Decode.Generic (genericDecodeJson)
import Data.Argonaut.Encode.Class (class EncodeJson)
import Data.Argonaut.Encode.Generic (genericEncodeJson)
import Data.ArrayBuffer.Class (class DecodeArrayBuffer, class DynamicByteLength, class EncodeArrayBuffer, genericByteLength, genericPutArrayBuffer, genericReadArrayBuffer)
import Data.Generic.Rep (class Generic)
import Data.Lens.Getter (view, (^.))
import Data.Lens.Iso (Iso', iso)
import Data.Lens.Prism (review)
import Data.Show.Generic (genericShow)
import Domination.Data.Pile (Pile)
import Domination.Data.StackEvaluation (StackExpression(..))
import Domination.Data.Var (Var)
import Domination.Data.Wire.Bonus (WireBonus)
import Domination.Data.Wire.Bonus as Bonus
import Domination.Data.Wire.Constraint (WireConstraint)
import Domination.Data.Wire.Constraint as Constraint
import Domination.Data.Wire.Filter (WireFilter)
import Domination.Data.Wire.Filter as Filter
import Domination.Data.Wire.Int (WireInt)
import Domination.Data.Wire.Int as Int
import Domination.Data.Wire.StackValue (WireStackValue)
import Domination.Data.Wire.StackValue as StackValue
import Util ((.^))

data WireStackExpression
  = WireStackChooseCards
    (Var (Array WireInt)) -- cards
    (Var WireFilter)
    (Var Pile)
    (Var WireConstraint)
  | WireStackDuplicate
  | WireStackDiscard
  | WireStackLength
  | WireStackAddN WireInt
  | WireStackDraw
  | WireStackTrash
  | WireStackNth WireInt
  | WireStackCostOf
  | WireStackMakeFilterCostUpTo
  | WireStackMakeFilterAnd
  | WireStackBind String
  | WireStackGainTo Pile
  | WireStackChooseCardFromSupply (Var String) (Var WireFilter)
  | WireStackEquals WireStackValue
  | WireStackIf
    (Array WireStackExpression)
    (Array WireStackExpression)
    (Array WireStackExpression)
  | WireStackPush WireStackValue
  | WireStackGainBonus WireBonus
  | WireStackOption (Var Boolean)

_toWire :: Iso' StackExpression WireStackExpression
_toWire = iso to fro where
  to = case _ of
    StackChooseCards { cards, filter, from, n } ->
      WireStackChooseCards
        (map (view Int._toWire) <$> cards)
        (view Filter._toWire <$> filter)
        from
        (view Constraint._toWire <$> n)
    StackDuplicate -> WireStackDuplicate
    StackDiscard -> WireStackDiscard
    StackLength -> WireStackLength
    StackAddN n -> WireStackAddN $ n ^. Int._toWire
    StackDraw -> WireStackDraw
    StackTrash -> WireStackTrash
    StackNth n -> WireStackNth $ n ^. Int._toWire
    StackCostOf -> WireStackCostOf
    StackMakeFilterCostUpTo -> WireStackMakeFilterCostUpTo
    StackMakeFilterAnd -> WireStackMakeFilterAnd
    StackBind s -> WireStackBind s
    StackGainTo pile -> WireStackGainTo pile
    StackChooseCardFromSupply { cardName, filter } ->
      WireStackChooseCardFromSupply
        cardName
        (view Filter._toWire <$> filter)
    StackEquals value -> WireStackEquals $ value ^. StackValue._toWire
    StackIf { condition, following, otherwise } -> WireStackIf
      (view _toWire <$> condition)
      (view _toWire <$> following)
      (view _toWire <$> otherwise)
    StackPush value -> WireStackPush $ value ^. StackValue._toWire
    StackGainBonus bonus -> WireStackGainBonus
      $ bonus ^. Bonus._toWire
    StackOption var -> WireStackOption var

  fro = case _ of
    WireStackChooseCards cards filter from n ->
      StackChooseCards
        { cards: map (review Int._toWire) <$> cards
        , filter: review Filter._toWire <$> filter
        , from
        , n: review Constraint._toWire <$> n
        }
    WireStackDuplicate -> StackDuplicate
    WireStackDiscard -> StackDiscard
    WireStackLength -> StackLength
    WireStackAddN n -> StackAddN $ n .^ Int._toWire
    WireStackDraw -> StackDraw
    WireStackTrash -> StackTrash
    WireStackNth n -> StackNth $ n .^ Int._toWire
    WireStackCostOf -> StackCostOf
    WireStackMakeFilterCostUpTo -> StackMakeFilterCostUpTo
    WireStackMakeFilterAnd -> StackMakeFilterAnd
    WireStackBind s -> StackBind s
    WireStackGainTo pile -> StackGainTo pile
    WireStackChooseCardFromSupply cardName filter ->
      StackChooseCardFromSupply
        { cardName
        , filter: review Filter._toWire <$> filter
        }
    WireStackEquals value -> StackEquals $ value .^ StackValue._toWire
    WireStackIf condition following otherwise -> StackIf
      { condition: review _toWire <$> condition
      , following: review _toWire <$> following
      , otherwise: review _toWire <$> otherwise
      }
    WireStackPush value -> StackPush $ value .^ StackValue._toWire
    WireStackGainBonus bonus -> StackGainBonus
      $ bonus .^ Bonus._toWire
    WireStackOption var -> StackOption var

derive instance genericWireStackExpression
  :: Generic WireStackExpression _

derive instance eqWireStackExpression :: Eq WireStackExpression

instance showWireStackExpression :: Show WireStackExpression where
  show wireChoice = genericShow wireChoice

instance encodeJsonWireStackExpression
  :: EncodeJson WireStackExpression where
  encodeJson a = genericEncodeJson a

instance decodeJsonWireStackExpression
  :: DecodeJson WireStackExpression where
  decodeJson a = genericDecodeJson a

instance dynamicByteLengthWireStackExpression
  :: DynamicByteLength WireStackExpression where
  byteLength x = genericByteLength x

instance encodeArrayBuffeWireStackExpression
  :: EncodeArrayBuffer WireStackExpression where
  putArrayBuffer x = genericPutArrayBuffer x

instance decodeArrayBuffeWireStackExpression
  :: DecodeArrayBuffer WireStackExpression where
  readArrayBuffer x = genericReadArrayBuffer x
