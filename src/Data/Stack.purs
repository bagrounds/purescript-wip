--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| A set of data types, functions, and lenses for working with stacks of cards in a card game.
--|
--| ### Key Concepts
--| * **Data Structures**: Understanding the `Stack` data structure and its conversion to and from other data structures (e.g., `Array Card`).
--| * **Lens**: Familiarity with the `Lens` module, including creating and using lenses to access and modify fields in a `Stack` record.
--| * **Iso**: Understanding Iso's (Isomorphic Functions) role in converting between different data representations.
module Domination.Data.Stack where

import Prelude

import Control.Monad.Error.Class (class MonadError)
import Data.Array (catMaybes, concatMap, filter, foldr, head, nub, replicate, (:))
import Data.Array.NonEmpty (NonEmptyArray)
import Data.Array.NonEmpty as NonEmpty
import Data.Foldable (length)
import Data.Lens.Iso (Iso', iso)
import Data.Lens.Lens (Lens, Lens', lens')
import Data.Lens.Record (prop)
import Data.Lens.Setter ((%~))
import Data.Symbol (SProxy(..))
import Data.Tuple (Tuple(..))
import Domination.Data.Card (Card)
import Domination.Data.Cards as Cards
import Domination.Data.Points (Points)
import Domination.Data.Points as Points
import Util (assert, decOver)

type Stack =
  { card :: Card
  , count :: Int
  }

toCards :: Stack -> Array Card
toCards { count, card } = replicate count card

fromCards :: NonEmptyArray Card -> Stack
fromCards cards = { card: NonEmpty.head cards, count: length cards }

fromPotentiallyEmptyCards :: Card -> Array Card -> Stack
fromPotentiallyEmptyCards card cards = { card, count: length cards }

_toCards :: Card -> Iso' Stack (Array Card)
_toCards card = iso toCards (fromPotentiallyEmptyCards card)

_card
  :: forall a b r
  . Lens { card :: a | r } { card :: b | r } a b
_card = prop (SProxy :: SProxy "card")

_count
  :: forall a b r
  . Lens { count :: a | r } { count :: b | r } a b
_count = prop (SProxy :: SProxy "count")

take :: Stack -> Stack
take = decOver _count

isEmpty :: Stack -> Boolean
isEmpty = _.count >>> (_ <= 0)

assertNotEmpty :: forall m. MonadError String m => Stack -> m Stack
assertNotEmpty = assert (_.count >>> (_ > zero)) "stack is empty!"

new :: Card -> Int -> Stack
new card count = { card, count }

_fromCard :: Lens' Card Stack
_fromCard = lens' f
  where
    f :: Card -> Tuple Stack (Stack -> Card)
    f card = Tuple (new card 1) (_.card)

_stacksFromCardsIso :: Iso' (Array Card) (Array Stack)
_stacksFromCardsIso = iso stackCards stacksToCards

_stacksToCardsIso :: Iso' (Array Stack) (Array Card)
_stacksToCardsIso = iso stacksToCards stackCards

stacksToCards :: Array Stack -> Array Card
stacksToCards = concatMap toCards

sortByStacks :: Array Card -> Array Card
sortByStacks = stacksToCards <<< stackCards

_stacksFromCards :: Lens' (Array Card) (Array Stack)
_stacksFromCards = lens' f
  where
    f :: (Array Card) -> Tuple (Array Stack) (Array Stack -> Array Card)
    f cards = Tuple (stackCards cards) stacksToCards

stackCards :: Array Card -> Array Stack
stackCards cards = catMaybes (foldr f [] names)
  where
    names = nub $ _.name <$> cards
    f name stacks =
      ({ card: _, count: length cards' } <$> head cards')
        : stacks
      where
        cards' = (_.name >>> (_ == name)) `filter` cards

upgrade :: Stack -> Stack
upgrade = _card %~ Cards.upgrade

points :: Stack -> Points
points { card, count } = card.victoryPoints * Points.points count

positivePoints :: Stack -> Points
positivePoints = max zero <<< points

negativePoints :: Stack -> Points
negativePoints = min zero <<< points
