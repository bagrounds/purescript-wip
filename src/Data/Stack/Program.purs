--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| PureScript module for working with programs as stack operations.
--|
--| ### Key Concepts
--| * **Category Theory**: Understanding concepts like categories, semigroupoids, braided categories, monoidal products, and cartesian products.
--| * **Monoidal Product**: Concepts related to a type with both left and right adjoints, including the first and second functors.
--| * **Stack Functions**: Evaluating operations on a stack data structure, involving primitive functions.
module Data.Stack.Program where

import Control.Category (class Category, (<<<))
import Control.Semigroupoid (class Semigroupoid)
import Data.BraidedCategory (class BraidedCategory, swap)
import Data.Cartesian (class Cartesian, duplicate, extractLeft, extractRight)
import Data.MonoidalProduct (class MonoidalProduct, first, second)
import Data.Stack.Function (StackFunction(..), evalStackFunction)
import Data.Stack.Operation (pop, primitive, push)
import Data.Stack.Operations (Operations(..), cons, evalOperations, nil)
import Data.Stack.Primitive (Primitive)
import Data.Tuple (Tuple)

newtype Program a b =
  Program (forall z. Operations (Tuple a z) (Tuple b z))

instance semigroupoidProgram :: Semigroupoid Program where
  compose (Program f) (Program g) = Program (f <<< g)

instance categoryProgram :: Category Program where
  identity = Program nil

instance braidedCategoryProgram
  :: BraidedCategory Program where
  swap = Program (Operations (first swap))

instance monoidalProductProgram
  :: MonoidalProduct Program where
  first (Program operations) =
    Program (pop `cons` nil <<< push `cons` operations)
  second g = swap <<< first g <<< swap
  cross f g = first f <<< second g

instance cartesianProgram :: Cartesian Program where
  extractLeft = primitiveProgram extractLeft
  extractRight = primitiveProgram extractRight
  duplicate = primitiveProgram duplicate

programFunction :: forall a b. Program a b -> StackFunction a b
programFunction (Program operations) =
  StackFunction (evalOperations operations)

evalProgram :: forall a b. Program a b -> a -> b
evalProgram = evalStackFunction <<< programFunction

primitiveProgram :: forall a b. Primitive a b -> Program a b
primitiveProgram p = Program (primitive p `cons` nil)
