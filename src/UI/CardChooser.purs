--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| A Halogen component for displaying a card chooser UI with toggleable cards, including functionality for resolving choices.
--|
--| ### Key Concepts
--| * **Halogen Component**: A Halogen component that manages a card chooser UI.
--| * **State Management**: The component maintains an array of tuples representing cards with their selection status.
--| * **Action Handling**: The component handles actions such as toggling card selection and resolving the game state.
module Domination.UI.CardChooser where

import Prelude

import Data.Array (filter)
import Data.Foldable (length)
import Data.FunctorWithIndex (mapWithIndex)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..), fst, snd)
import Domination.Capability.Dom (class Dom)
import Domination.Capability.Log (class Log)
import Domination.Data.Card (Card)
import Domination.Data.Choice (Choice)
import Domination.Data.Game (Game)
import Domination.Data.Pile (Pile)
import Domination.Data.Pile as Pile
import Domination.Data.Player (Player)
import Domination.Data.Player as Player
import Domination.Data.Stack (stacksToCards)
import Domination.UI.Card as Card
import Domination.UI.Css as Css
import Domination.UI.DomSlot (DomSlot)
import Domination.UI.Util (h2__)
import Halogen as H
import Halogen.Component (Component, ComponentSlot)
import Halogen.Data.Slot (Slot)
import Halogen.HTML (HTML)
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

type State = Array (Tuple Card Boolean)

data Action
  = Toggle Int
  | Done

type ChildrenWidget t26 t25 r m =
  ComponentSlot
  ( description :: Slot t26 t25 DomSlot | r )
  m
  Action

type RenderChoice t26 t25 t29 m
  = Choice
    -> Maybe
      { buttonText :: HTML (ChildrenWidget t26 t25 t29 m) Action
      , title :: HTML (ChildrenWidget t26 t25 t29 m) Action
      }

type CanToggle = { selected :: Boolean, total :: Int } -> Boolean

type ComponentSpec t26 t25 t29 m =
  { baseSlotNumber :: Int -> DomSlot
  , canToggle :: CanToggle
  , choice :: Choice
  , pile :: Pile
  , player :: Player
  , renderChoice :: RenderChoice t26 t25 t29 m
  , resolve :: Maybe (Array Int) -> Choice
  , state :: Game
  }

component
  :: forall input query t26 t25 t29 m
  . Ord DomSlot
  => Dom m
  => Log m
  => ComponentSpec t26 t25 t29 m
  -> Component query input Choice m
component
  { baseSlotNumber
  , renderChoice
  , canToggle
  , resolve
  , state
  , player
  , pile
  } = H.mkComponent { initialState, render, eval }
  where
  initialState :: forall a. a -> Array (Tuple Card Boolean)
  initialState _ = (\x -> Tuple x false) <$> cards
    where
      cards = case pile of
        Pile.AtPlay -> player.atPlay
        Pile.Hand -> player.hand
        Pile.Discard -> player.discard
        Pile.Buying -> player.buying
        Pile.Discarding -> player.toDiscard
        Pile.Deck -> player.deck
        Pile.Trash -> state.trash
        Pile.Supply -> stacksToCards state.supply

  render xs =
    case Player.firstChoice player >>= renderChoice of
      Just { title, buttonText } -> HH.div_ $
        [ title
        , HH.p_
          [ HH.button
            [ HP.class_ Css.resolveChoice
            , HE.onClick \_ -> Done
            ]
            [ buttonText ]
          ]
        ]
        <> renderCardToTrash `mapWithIndex` xs

      Nothing -> h2__ "Something has gone terribly wrong!"

  eval = H.mkEval H.defaultEval
    { handleAction = case _ of
      Toggle i -> do
        xs <- H.get
        let total = length $ snd `filter` xs
        H.modify_
          $ mapWithIndex
            \j (Tuple c selected) -> Tuple c
              $ if i == j
                && canToggle { selected, total }
                then not selected
                else selected

      Done -> ((resolve <<< resolution) <$> H.get) >>= H.raise
    }

    where
      resolution :: State -> Maybe (Array Int)
      resolution xs =
        Just $ map snd
        $ filter fst
        $ (\i (Tuple _ b) -> (Tuple b i)) `mapWithIndex` xs

  renderCardToTrash cardIndex (Tuple card selected) =
    Card.render onClick extraClasses card (baseSlotNumber cardIndex)
    where
      onClick _ = Toggle cardIndex
      extraClasses =
        [ if selected
          then Css.toTrash
          else Css.toKeep
        ]
