--| ## AI Generated Module Summary (llama3.2:3b)
--|
--| ### Description
--| Class names defined for a CSS UI component, likely related to a card game.
--|
--| ### Key Concepts
--| * CSS class naming conventions
--| * Modular organization of UI classes
--| * Common patterns for representing game-related states and components

module Domination.UI.Css where

import Halogen (ClassName(..))

stats :: ClassName
stats = ClassName "stats"
stat :: ClassName
stat = ClassName "stat"
supply :: ClassName
supply = ClassName "supply"
hand :: ClassName
hand = ClassName "hand"
table :: ClassName
table = ClassName "table"
handTitle :: ClassName
handTitle = ClassName "hand-title"
handInfos :: ClassName
handInfos = ClassName "hand-infos"
handInfo :: ClassName
handInfo = ClassName "hand-info"
play :: ClassName
play = ClassName "play"
playTitle :: ClassName
playTitle = ClassName "play-title"
buying :: ClassName
buying = ClassName "buying"
buyingTitle :: ClassName
buyingTitle = ClassName "buying-title"
deck :: ClassName
deck = ClassName "deck"
discard :: ClassName
discard = ClassName "discard"
card :: ClassName
card = ClassName "card"
curseCard :: ClassName
curseCard = ClassName "curse-card"
attackCard :: ClassName
attackCard = ClassName "attack-card"
actionCard :: ClassName
actionCard = ClassName "action-card"
reactionCard :: ClassName
reactionCard = ClassName "reaction-card"
treasureCard :: ClassName
treasureCard = ClassName "treasure-card"
victoryCard :: ClassName
victoryCard = ClassName "victory-card"
cardName :: ClassName
cardName = ClassName "card-name"
cardCards :: ClassName
cardCards = ClassName "card-cards"
cardActions :: ClassName
cardActions = ClassName "card-actions"
cardBuys :: ClassName
cardBuys = ClassName "card-buys"
cardTreasure :: ClassName
cardTreasure = ClassName "card-treasure"
cardVictoryPoints :: ClassName
cardVictoryPoints = ClassName "card-victory-points"
cardCost :: ClassName
cardCost = ClassName "card-cost"
cardText :: ClassName
cardText = ClassName "card-text"
stack :: ClassName
stack = ClassName "stack"
stackCard :: ClassName
stackCard = ClassName "stack-card"
stackCount :: ClassName
stackCount = ClassName "stack-count"
active :: ClassName
active = ClassName "active"
waiting :: ClassName
waiting = ClassName "waiting"
inactive :: ClassName
inactive = ClassName "inactive"
canBuy :: ClassName
canBuy = ClassName "can-buy"
cantBuy :: ClassName
cantBuy = ClassName "cant-buy"
canPlay :: ClassName
canPlay = ClassName "can-play"
cantPlay :: ClassName
cantPlay = ClassName "cant-play"
nextPhase :: ClassName
nextPhase = ClassName "next-phase"
resolveChoice :: ClassName
resolveChoice = ClassName "resolve-choice"
toTrash :: ClassName
toTrash = ClassName "to-trash"
toKeep :: ClassName
toKeep = ClassName "to-keep"
description :: ClassName
description = ClassName "description"
toolTip :: ClassName
toolTip = ClassName "tool-tip"
toggle :: ClassName
toggle = ClassName "toggle"
supplyToggle :: ClassName
supplyToggle = ClassName "supply-toggle"
showing :: ClassName
showing = ClassName "showing"
collapsed :: ClassName
collapsed = ClassName "collapsed"
settingsButton :: ClassName
settingsButton = ClassName "settings-button"
backButton :: ClassName
backButton = ClassName "back-button"
backButtonContainer :: ClassName
backButtonContainer = ClassName "back-button-container"
settingsMenu :: ClassName
settingsMenu = ClassName "settings-menu"
settings :: ClassName
settings = ClassName "settings"
usernameInput :: ClassName
usernameInput = ClassName "username-input"
announceInput :: ClassName
announceInput = ClassName "announce-input"
connections :: ClassName
connections = ClassName "connections"
statsContainer :: ClassName
statsContainer = ClassName "stats-container"
drawAttention :: ClassName
drawAttention = ClassName "draw-attention"
kingdom :: ClassName
kingdom = ClassName "kingdom"
newGameButton :: ClassName
newGameButton = ClassName "new-game-button"
loadGameButton :: ClassName
loadGameButton = ClassName "load-game-button"
toggleLongGame :: ClassName
toggleLongGame = ClassName "toggle-long-game"
check :: ClassName
check = ClassName "check"

incrementer :: ClassName
incrementer = ClassName "incrementer"
dialogue :: ClassName
dialogue = ClassName "dialogue"
domination :: ClassName
domination = ClassName "domination"
title :: ClassName
title = ClassName "title"
playerName :: ClassName
playerName = ClassName "player-name"
iteration :: ClassName
iteration = ClassName "iteration"
controls :: ClassName
controls = ClassName "controls"
undo :: ClassName
undo = ClassName "undo"
currentPlayer :: ClassName
currentPlayer = ClassName "current-player"

icon :: ClassName
icon = ClassName "icon"
actions :: ClassName
actions = ClassName "actions"
buys :: ClassName
buys = ClassName "buys"
money :: ClassName
money = ClassName "money"
points :: ClassName
points = ClassName "points"
cards :: ClassName
cards = ClassName "cards"
